# Changelog

## [1.3.2] - 2024-11-18
### Fixed
- Issue with Vite builds.
## [1.3.1] - 2024-10-31
### Security
- Updated postcss to latest. [Vulnerability](https://github.com/advisories/GHSA-7fh5-64p2-3v2j)
- Updated libphonenumber-js to latest.
## [1.3.0] - 2023-08-11
### Added
- Internationalization. Component now accepts a `language` property (default value "en")
which translates all text content. Based on the Wikipedia articles regarding the active
languages of the various continents, the component has been translated into every language
with 5 million or more native speakers. 118 total languages are supported.
### Fixed
- Hardened handling of developer input. Lowercase country codes as well as uppercase language codes
work as reasonably expected, while truly nonsense input is ignored.
### Security
- Updated libphonenumber-js to latest
## [1.2.1] - 2023-08-07
### Fixed
- Improve documentation
- Incompatibility with AZERTY and other international keyboard layouts addressed
- Keyboard displayed on mobile is now the telephone number pad
## [1.2.0] - 2023-02-22
### Changed
- Component will attempt to interpret an non E164 `value` string in the context
of the selected `default-country` when mounted before clearing the input
### Security
- Updated libphonenumber-js to latest
- Peer dependencies for Vue and Vuetify have been verified to work with much
earlier versions. Dialed them both back to 2.x.x, which should clear most NPM
warnings
## [1.1.3] - 2023-02-16
### Fixed
- Input would not clear if v-model was emptied or nulled programatically
## [1.1.2] - 2023-02-16
### Fixed
- Currently selected country was not highlighted in the list
## [1.1.1] - 2023-02-16
### Fixed
- Unsupported use of nullsafe operators
## [1.1.0] - 2023-01-16
### Added
- Option to restrict input to VOIP numbers
- Console warning when using mutually exclusive number type filters
- Support for users to enter their country code into the text field and have
the country code setting automatically change (even when the country selector
is disabled)
### Fixed
- Copy and pasting a full E164 phone number string did not parse correctly
- Copy and pasting a full E164 phone number string did not change the selected
country
## [1.0.7] - 2022-10-17
### Fixed
- Demo GIF link can't be relative to show up in NPM
## [1.0.6] - 2022-10-17
### Added
- Exposed the underlying phone number object via the `phone-number` event  
- Demo GIF
### Fixed
- Cursor is no longer pointer for the country selector when the input is non
interactive  
- Potential issue with CSS namespacing  
- Word wrap in the country code selector if the input is especially narrow
## [1.0.5] - 2022-09-11
### Added
- A changelog  
- Installation instructions for one-time use in a component  
- A development Vue app for faster testing
### Fixed
- Country code selector append icon responds appropriately to `disabled` and
`readonly` states  
- Dark theme appearance of the country selector

## [1.0.4] - 2022-09-08
### Fixed
- Issue where the label of an outlined field would clip into the outline after
changing countries with the country selector  
- Issue where initializing the component with a value would fail to set the
current country if different from the default country

## [1.0.3] - 2022-09-04
### Fixed
- Passthrough of the `prefix` property

## [1.0.2] - 2022-09-04
### Fixed
- Validation compatibility with `v-form` parent tags

## [1.0.1] - 2022-09-03
### Fixed
- Tweaked build configuration to allow for successful installation from NPM

## [1.0.0] - 2022-09-03
- Initial release