import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import VPhoneNumber from '~/VPhoneNumber.vue';

Vue.use(Vuetify);
Vue.component('v-phone-number', VPhoneNumber);

const opts = {
  icons: {
    iconfont: 'mdi',
  },
};

export default new Vuetify(opts);
