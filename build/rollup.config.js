import alias from '@rollup/plugin-alias';
import commonjs from '@rollup/plugin-commonjs';
import vue from 'rollup-plugin-vue';
import vuetify from 'rollup-plugin-vuetify';
import buble from '@rollup/plugin-buble';
import scss from 'rollup-plugin-scss';

export default {
  input: 'src/wrapper.js',
  external: [
    'vue',
    'vuetify/lib',
    'libphonenumber-js/max',
  ],
  output: {
    sourcemap: false,
    name: 'VPhoneNumber',
    exports: 'named',
    inlineDynamicImports: true,
    globals: {
      'vue': 'Vue',
      'vuetify/lib': 'Vuetify',
      'libphonenumber-js/max': 'libphonenumber',
    },
  },
  plugins: [
    alias({
      entries: [
        { find: /^~\/(.*)$/, replacement: '../src/$1'},
      ],
    }),
    vue({
      compileTemplate: true,
    }),
    vuetify({
      include: 'src/**',
    }),
    commonjs(),
    scss(),
    buble({
      objectAssign: 'Object.assign',
      exclude: '**/*.css',
    }),
  ],
};