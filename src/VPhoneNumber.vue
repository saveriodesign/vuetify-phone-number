<template>
  <div class="v-phone-number">
    <v-text-field
        ref="number_input"
        type="tel"
        class="v-phone-number__number-input"
        :value="nationalNumber"
        :rules="rules"
        v-bind="attrs"
        v-on="listeners"
        @blur="onBlur"
        @focus="onFocus"
        @input="onInput"
        @keydown="onKeydown">
      <template v-if="! disableCountrySelect" #prepend-inner>
        <country-selector
            v-bind="countrySelectorAttributes"
            v-model="lazyCountryCode"
            @select="() => $refs.number_input.focus()"
            @input="(v) => $emit('update:country-code', v)"
            @click:prepend-inner="(e) => $emit('click:prepend-inner', e)" />
      </template>
      <slot v-for="(_, name) in slots" :name="name" :slot="name" />
      <template v-for="(_, name) in scopedSlots" :slot="name" slot-scope="scope">
        <slot :name="name" v-bind="scope" />
      </template>
    </v-text-field>
  </div>
</template>

<script>
import { AsYouType, parsePhoneNumber } from 'libphonenumber-js/max';

import CountrySelector from '~/CountrySelector.vue';
import { blankPhoneNumber, validCountryCodesMap, getCountryCode } from '~/lib/core';
import i18n from '~/lib/i18n';
import _ from '~/lib/lodash';

export default {
  name: 'v-phone-number',
  inheritAttrs: false,

  components: { CountrySelector },

  props: {
    // bound model
    value: {
      type: String,
      default: () => '',
    },
    // localization
    language: {
      type: String,
      default: () => 'en',
    },
    // country code data settings
    defaultCountry: {
      type: String,
      default: () => 'US',
    },
    includedCountries: {
      type: Array,
      default: () => [],
    },
    excludedCountries: {
      type: Array,
      default: () => [],
    },
    // country code display settings
    disableCountrySelect: {
      type: Boolean,
      default: () => false,
    },  
    hideFlag: {
      type: Boolean,
      default: () => false,
    },
    hideCallingCode: {
      type: Boolean,
      default: () => false,
    },
    showCountryCode: {
      type: Boolean,
      default: () => false,
    },
    // validation settings
    voip: {
      type: Boolean,
      default: () => false,
    },
    mobile: {
      type: Boolean,
      default: () => false,
    },
    landline: {
      type: Boolean,
      default: () => false,
    },
    strict: {
      type: Boolean,
      default: () => false,
    },
    required: {
      type: Boolean,
      default: () => false,
    },
  },

  data: () => ({
    lazyCountryCode: '',
    nationalNumber: '',
  }),

  computed: {
    // passthroughs
    attrs () {
      const keys = this.disableCountrySelect
        ? [ 'counter', 'counterValue', 'hideSpinButtons', 'type', 'rules' ]
        : [ 'counter', 'counterValue', 'hideSpinButtons', 'type', 'rules', 'prependInnerIcon' ];
      return _.omit(this.$attrs, keys);
    },
    listeners () {
      const keys = this.disableCountrySelect
        ? [ 'blur', 'focus', 'input', 'keydown' ]
        : [ 'blur', 'focus', 'input', 'keydown', 'click:prepend-inner' ];
      return _.omit(this.$listeners, keys);
    },
    slots () {
      const keys = this.disableCountrySelect
        ? [ 'counter', 'progress' ]
        : [ 'counter', 'progress', 'prepend-inner' ];
      return _.omit(this.$slots, keys);
    },
    scopedSlots () {
      const keys = this.disableCountrySelect
        ? [ 'counter', 'progress' ]
        : [ 'counter', 'progress', 'prepend-inner' ];
      return _.omit(this.$scopedSlots, keys);
    },
    countrySelectorAttributes () {
      return {
        // forward options
        language: this._language,
        defaultCountry: this._defaultCountry,
        includedCountries: this.includedCountries.reduce(this.countryListFilter, []),
        excludedCountries: this.excludedCountries.reduce(this.countryListFilter, []),
        hideFlag: this.hideFlag,
        hideCallingCode: this.hideCallingCode,
        showCountryCode: this.showCountryCode,
        // pick vuetify options
        ... _.pick(this.$attrs, [ 'color', 'disabled', 'prependInnerIcon', 'readonly', 'success' ]),
      };
    },
    // data
    _defaultCountry () {
      return this.defaultCountry.toUpperCase();
    },
    _language () {
      return this.language.replace(/[-_ ].*$/, '').toLowerCase();
    },
    countryCode () {
      return this.disableCountrySelect ? this._defaultCountry : this.lazyCountryCode;
    },
    phoneNumber () {
      try { return parsePhoneNumber(this.nationalNumber, this.countryCode); }
      catch (e) { return blankPhoneNumber; }
    },
    // validation
    numberIsValid () {
      return this.strict
        ? this.phoneNumber.isValid()
        : this.phoneNumber.isPossible();
    },
    numberIsVoip () {
      return (this.phoneNumber.getType() || '').includes('VOIP');
    },
    numberIsMobile () {
      return (this.phoneNumber.getType() || '').includes('MOBILE');
    },
    numberIsLandline () {
      return (this.phoneNumber.getType() || '').includes('FIXED');
    },
    numberTypeRestrictionValid () {
      return [ this.voip, this.mobile, this.landline ]
        .filter(v => v)
        .length <= 1;
    },
    rules () {
      const rules = [ (v) => ! v || this.numberIsValid || i18n('error.phone', this._language) ];
      if (this.required) {
        rules.unshift((v) => !! v || i18n('error.required', this._language));
      }
      // these types are mutually exclusive
      if (this.voip) {
        rules.push((v) => ! v || this.numberIsVoip || i18n('error.voip', this._language));
      } else if (this.mobile) {
        rules.push((v) => ! v || this.numberIsMobile || i18n('error.mobile', this._language));
      } else if (this.landline) {
        rules.push((v) => ! v || this.numberIsLandline || i18n('error.fixed', this._language));
      }
      return rules;
    },
  },

  watch: {
    value (value) {
      if (! value) {
        this.nationalNumber = '';
        return;
      }
      try {
        let phoneNumber = parsePhoneNumber(value);
        if (! phoneNumber.isEqual(this.phoneNumber)) {
          this.nationalNumber = phoneNumber.format('NATIONAL');
          this.lazyCountryCode = getCountryCode(phoneNumber);
        }
      }
      catch (e) {
        // don't care
      }
    },
    lazyCountryCode () {
      if (this.nationalNumber) {
        this.emit(this.nationalNumber);
      }
      if (this.$refs.number_input.$el.classList.value.includes('v-text-field--outlined')) {
        this.$nextTick(() => window.dispatchEvent(new Event('resize')));
      }
    },
    numberTypeRestrictionValid (v) {
      if (! v) {
        console.warn('VPhoneNumber: The properties `voip`, `mobile`, and `landline` are mutually exclusive. Setting more than one of these to true will cause the component to behave unpredictably.');
      }
    },
  },

  methods: {
    // method passthroughs
    blur (... args) {
      return this.$refs.number_input.blur(... args);
    },
    focus (... args) {
      return this.$refs.number_input.focus(... args);
    },
    validate (... args) {
      return this.$refs.number_input.validate(... args);
    },
    // event reponses
    onCountryCodeUpdate (event) {
      this.$emit('update:country-code', event);
      this.$refs.number_input.focus();
    },
    onKeydown (event) {
      this.$emit('keydown', event);
      const isInput = /^[0-9+()\- ]$/.test(event.key);
      const isNav = /^((Arrow(Left|Up|Right|Down))|Home|End|Backspace|Delete|Tab|Enter)$/.test(event.key);
      const isFunction = /^F(1[012]|[1-9])$/.test(event.key);
      const isSuper = /((^(OS|Alt|Shift|Control))|Lock)$/.test(event.key);
      if (! (isInput || isNav || isFunction || isSuper || event.ctrlKey || event.metaKey)) {
        event.preventDefault();
      }
    },
    onInput (input) {
      // skip if emptied
      if (! input) {
        this.$emit('input', input);
        this.$emit('phone-number', blankPhoneNumber);
        return;
      }
      try {
        let phoneNumber = parsePhoneNumber(input);
        if (phoneNumber && phoneNumber.country && phoneNumber.country !== this.countryCode) {
          this.lazyCountryCode = phoneNumber.country;
          input = input.replace(`+${phoneNumber.countryCallingCode}`, '');
        }
      }
      catch (e) {
        // don't care if the parse fails
      }
      this.emit(input.replace(/[^\s-()0-9+]/g, ''));
    },
    onFocus (event) {
      this.$emit('focus', event);
      if (this.$refs.number_input.$el.classList.value.includes('v-text-field--outlined')) {
        window.dispatchEvent(new Event('resize'));
      }
    },
    onBlur (event) {
      this.$emit('blur', event);
      this.validate();
    },
    countryListFilter (list, item) {
      const fixed = item.toUpperCase();
      if (validCountryCodesMap[fixed]) {
        list.push(fixed);
      }
      return list;
    },
    emit (value) {
      this.nationalNumber = (new AsYouType(this.countryCode)).input(value);
      this.$emit('input', this.phoneNumber.number);
      this.$emit('phone-number', this.phoneNumber);
    },
  },

  mounted () {
    let phoneNumber;
    try {
      phoneNumber = parsePhoneNumber(this.value);
      this.nationalNumber = phoneNumber.nationalNumber;
      this.lazyCountryCode = getCountryCode(phoneNumber);
    }
    catch (e) {
      try {
        phoneNumber = parsePhoneNumber(this.value, this._defaultCountry);
        this.nationalNumber = phoneNumber.nationalNumber;
        this.lazyCountryCode = this._defaultCountry;
      }
      catch (e2) {
        // don't care
      }
    }
    if (this.$refs.number_input.$el.classList.value.includes('v-text-field--outlined')) {
      this.$nextTick(() => window.dispatchEvent(new Event('resize')));
    }
  },
};
</script>
