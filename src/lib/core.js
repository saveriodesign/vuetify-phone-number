import { getCountries, Metadata } from'libphonenumber-js';

const metadata = (new Metadata).metadata;

/** Approximate empty PhoneNumber object to prevent console errors */
const blankPhoneNumber = Object.freeze({
  number: '',
  nationalNumber: '',
  isValid: () => false,
  isPossible: () => false,
  getType: () => undefined,
});

/** Country code-keyed object of truthy values */
const validCountryCodesMap = Object.freeze(getCountries().reduce((map, key) => {
  map[key] = 1;
  return map;
}, {}));

/** Data for the country code selector */
const countryData = Object.freeze(getCountries().map(key => ({
  countryCode: key,
  callingCode: metadata.countries[key][0],
})));

/**
 * Fetch localized data for the country code selector
 * @param {string} locale 
 * @returns {{name: string, flag: string, countryCode: string, callingCode: string}[]}
 */
function getCountryCodeSelectorList(locale) {
  let intl;
  try { intl = new Intl.DisplayNames([locale], { type: 'region' }); }
  catch (e) { intl = new Intl.DisplayNames(['en'], { type: 'region' }); }
  return getCountries().map(key => ({
    name: intl.of(key),
    flag: getFlagEmoji(key),
    countryCode: key,
    callingCode: metadata.countries[key][0],
  }));
}

/**
 * Get the [ISO 3166](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes)
 * country code from a PhoneNumber object
 * @param {PhoneNumber} phoneNumber 
 * @returns {string}
 */
function getCountryCode(phoneNumber) {
  return phoneNumber.country
    ? phoneNumber.country
    : countryData.find(item => item.callingCode === phoneNumber.countryCallingCode).countryCode;
}

/**
 * Get the national flag emoji of a country by its 2-digit
 * [ISO 3166](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes)
 * country code
 * @param {string} countryCode 
 * @returns {string}
 */
function getFlagEmoji(countryCode) {
  return String.fromCodePoint(...countryCode.toUpperCase().split('').map(char => 127397 + char.charCodeAt()));
}

export {
  blankPhoneNumber,
  getCountryCodeSelectorList,
  getCountryCode,
  getFlagEmoji,
  validCountryCodesMap,
};
