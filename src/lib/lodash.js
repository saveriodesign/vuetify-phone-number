export default {
  /**
   * Clone an object
   * @template T
   * @param {T} entity 
   * @returns {T}
   */
  cloneDeep(entity) {
    if (typeof entity === 'object') {
      let ret;
      if (Array.isArray(entity)) {
        ret = [];
      } else if (entity instanceof Date) {
        return new Date(entity);
      } else if (entity !== null) {
        ret = {};
      // return null
      } else {
        return entity;
      }
      // recurse!
      for (const key in entity) {
        ret[key] = this.cloneDeep(entity[key]);
      }
      return ret;
    } else {
      // return a primitive
      return entity;
    }
  },

  /**
   * Create a new object which only has the given properties
   * and methods of a given object
   * @param {any} entity 
   * @param {string[]} keys 
   * @returns {any}
   */
  pick(entity, keys) {
    return keys.reduce((ret, key) => {
      if (Object.prototype.hasOwnProperty.call(entity, key)) {
        ret[key] = this.cloneDeep(entity[key]);
      }
      return ret;
    }, {});
  },

  /**
   * Create a new object which does not have the given
   * properties or methods from a given object
   * @param {any} entity 
   * @param {string[]} keys 
   * @returns {any}
   */
  omit(entity, keys) {
    return Object.keys(entity).reduce((ret, key) => {
      if (! keys.includes(key)) {
        ret[key] = this.cloneDeep(entity[key]);
      }
      return ret;
    }, {});
  },
};
