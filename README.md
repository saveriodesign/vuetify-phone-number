# v-phone-number

Phone number form input visually compatible with Vuetify projects running on
Vue 2.x. Allows users to enter in their phone number in their localized, native
format while giving your app an [E164](https://en.wikipedia.org/wiki/E.164)
formatted string suitable for SMS and VoIP APIs.

## Demo

![Demo GIF](https://gitlab.com/saveriodesign/vuetify-phone-number/-/raw/master/public/demo.gif)

## Requirements

Proven with the following minimum versions of Vue and Vuetify.

* [vue](https://www.npmjs.com/package/vue/v/2.0.0) ^2
* [vuetify](https://www.npmjs.com/package/vuetify/v/2.0.0) ^2

## Installation

```bash
npm i -s v-phone-number
```

Add the component as normally to use in a single component or view

```js
import VPhoneNumber from 'v-phone-number';
// ...
export default {
  components: {
    VPhoneNumber,
    // ..
  },
};
```

To add `v-phone-number` to the entire app, register the component in either
`plugins/vuetify.js` or directly in `main.js`

```js
import VPhoneNumber from 'v-phone-number';
// ...
Vue.component('v-phone-number', VPhoneNumber);
```

## Usage

```html
<v-phone-number label="Phone Number" v-model="phoneNumber" />
```

### Props

[Most v-text-field props](https://vuetifyjs.com/en/api/v-text-field/#props) but
not:

* counter  
* counter-value  
* hide-spin-buttons  
* rules (use `landline`, `mobile`, `required` and `strict`)  
* type (hard-coded to `tel` for optimal mobile experience)

The following props are enabled, but will make the input look bad unless you
turn off the country selector or dig in with some unscoped CSS to support your
use case (see "Advanced" below).

* height  
* filled, label, placeholder, and persistent-placeholder simulataneously  
* prefix  
* reverse  
* solo-inverted  
* suffix

Additionally the following phone-specific properties are available.

Name | Type | Default | Description
---|---|---|---
`default-country`|`string`|`'US'`|[ISO 3166](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes) Country code to initialize the input with. If the input is created with a value, the country code of the E164 number given is always used.
`disable-country-select`|`boolean`|`false`|Hide the country selector entirely.
`excluded-countries`|`array`|`[]`|ISO 3166 country codes to exclude as choices for locality. Ignored if value is an empty array. If the input is created with a value, the country code of the E164 number given is always used.
`included-countries`|`array`|`[]`|The complete list of ISO 3166 country codes to inlude as choices for locality. Ignored if value is an empty array. If the input is created with a value, the country code of the E164 number given is always used.
`hide-calling-code`|`boolean`|`false`|Hide the calling code prefix of the selected country.
`hide-flag`|`boolean`|`false`|Hide the flag emoji of the selected country.
`landline`|`boolean`|`false`|Only accept fixed landline number.
`language`|`string`|`'en'`|ISO 639 language code of the component's content. Languages with fewer than five million native speakers as of August 2023 may not be supported. National locales have no effect.
`mobile`|`boolean`|`false`|Only accept mobile number.
`show-country-code`|`boolean`|`false`|Display the ISO 3166 country code of the selected country.
`strict`|`boolean`|`false`|If `false`, validation only checks there are enough digits in the phone number for the selected country. If `true`, the value itself will be checked (ie, no US area code begins with a zero).
`voip`|`boolean`|`false`|Only accept voice over IP number.

### Events

[All v-text-field events](https://vuetifyjs.com/en/api/v-text-field/#events)
and:

Name | Type | Description
---|---|---
`phone-number`|`PhoneNumber`|Underlying `PhoneNumber` object, see [libphonenumber-js](https://www.npmjs.com/package/libphonenumber-js) for structure
`update:country-code`|`string`|ISO 3166 country code of the selected country from the menu

### Slots

[Most v-text-field slots](https://vuetifyjs.com/en/api/v-text-field/#slots) but
not:

* counter  
* prepend-inner while `disable-country-select` is false  
* progress

### Advanced

This project uses BEM (Block Element Modifier) class hierarchy for its components,
allowing for precise control over the appearance of this input should the default
Vuetify style defaults be found wanting. The block is `v-phone-number`, and contains
the following elements:

* `country-code`
* `country-code-display`
* `country-code-search`
* `country-list`
* `number-input`

There are no modifiers to these classes.
