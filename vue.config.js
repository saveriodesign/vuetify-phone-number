/* eslint no-undef: 0 */
const path = require('path');
const { defineConfig } = require('@vue/cli-service');
module.exports = defineConfig({
  transpileDependencies: true,
  pages: {
    index: { entry: 'dev/main.js' },
  },
  configureWebpack: {
    resolve: {
      alias: {
        '~': path.resolve(__dirname, 'src'),
        '#': path.resolve(__dirname, 'dev'),
      },
    },
  },
});
